import React from 'react';
import './components/styles.css';

const YoutubeEmbed = ({ link }) => {
  return (
    <div className="video-responsive">
      <iframe
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        frameBorder="0"
        height="422"
        src={link}
        width="750"
      />
    </div>
  );
};

export default YoutubeEmbed;
