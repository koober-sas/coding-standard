import React from 'react';
import './components/styles.css';

const CommunityAuthor = ({ name, imageUrl, twitterUrl, gitUrl, description }) => {
  return (
    <>
      <h2 className="communitySection">About the community author</h2>
      <hr className="separator" />
      <div className="authorSection">
        <div className="authorImg">
          <img src={imageUrl} />
        </div>
        <div className="authorDetails">
          <div className="authorName">
            <strong>{name}</strong>
            {twitterUrl ? (
              <a href={twitterUrl} rel="noopener noreferrer" target="_blank">
                <img src="https://storage.googleapis.com/graphql-engine-cdn.hasura.io/learn-hasura/assets/social-media/twitter-icon.svg" />
              </a>
            ) : null}
            {gitUrl ? (
              <a href={gitUrl} rel="noopener noreferrer" target="_blank">
                <img src="https://upload.wikimedia.org/wikipedia/commons/1/18/GitLab_Logo.svg" />
              </a>
            ) : null}
          </div>
          <div className="authorDesc">{description}</div>
        </div>
      </div>
      <hr className="separator" />
    </>
  );
};

export default CommunityAuthor;
