import * as React from 'react';
import githubIconSrc from './components/images/github.svg';
import './components/styles.css';

const GithubLink = ({ link, text }) => {
  return (
    <a className="githubSection" href={link}>
      <img alt="github" className="githubIcon" src={githubIconSrc} />
      {text}
    </a>
  );
};

export default GithubLink;
