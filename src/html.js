/* eslint-disable react/no-danger, filenames/match-exported */
import React from 'react';
import PropTypes from 'prop-types';
import config from '../config';

export default class HTML extends React.Component {
  render() {
    return (
      <html {...this.props.htmlAttributes}>
        <head>
          <meta charSet="utf-8" />
          <meta content="ie=edge" httpEquiv="x-ua-compatible" />
          <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport" />
          {config.siteMetadata.ogImage ? <meta content={config.siteMetadata.ogImage} property="og:image" /> : null}
          <meta content="summary_large_image" property="twitter:card" />
          {config.siteMetadata.ogImage ? <meta content={config.siteMetadata.ogImage} property="twitter:image" /> : null}
          {config.siteMetadata.favicon ? (
            <link href={config.siteMetadata.favicon} rel="shortcut icon" type="image/svg" />
          ) : null}
          <link
            crossOrigin="anonymous"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
            rel="stylesheet"
          />
          <script
            crossOrigin="anonymous"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            src="https://code.jquery.com/jquery-3.3.1.min.js"
          />
          <script
            crossOrigin="anonymous"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
          />
          {this.props.headComponents}
        </head>
        <body {...this.props.bodyAttributes}>
          {this.props.preBodyComponents}
          <div key={`body`} dangerouslySetInnerHTML={{ __html: this.props.body }} id="___gatsby" />
          {this.props.postBodyComponents}
          <script
            dangerouslySetInnerHTML={{
              __html: `
            $(document).on('click','.navbar-collapse.in',function(e) {
              if( $(e.target).is('a') ) {
                $(this).collapse('hide');
              }
            });
            `,
            }}
          />
        </body>
      </html>
    );
  }
}

HTML.propTypes = {
  body: PropTypes.string,
  bodyAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  htmlAttributes: PropTypes.object,
  postBodyComponents: PropTypes.array,
  preBodyComponents: PropTypes.array,
};
