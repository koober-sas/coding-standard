import React from 'react';
import { Link as GatsbyLink } from 'gatsby';
import isAbsoluteUrl from 'is-absolute-url';

const Link = ({ to, ...properties }) =>
  isAbsoluteUrl(to) ? <a href={to} {...properties} /> : <GatsbyLink to={to} {...properties} />;

export default Link;
