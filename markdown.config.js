// @ts-check
const fs = require('fs');
const path = require('path');

module.exports = {
  callback() {
    // console.log('done')
  },
  transforms: {
    INDEX: function INDEX(content, _options, config) {
      const defaults = {
        dir: '.', // eslint-disable-line unicorn/prevent-abbreviations
        verbose: false,
        bullet: '*',
      };
      const options = Object.assign({}, defaults, _options);

      const packagesDirectory = path.resolve(path.dirname(config.originalPath), options.dir);

      function getTitle(filePath) {
        // TODO: Improve by reading the title in the body of the document
        const [header, ...suffix] = filePath.replace('.md', '').split('--');
        const title = suffix.join('--').replace(/-/g, ' ');

        return title.length > 0 ? `${header}. ${title}` : header;
      }

      return fs
        .readdirSync(packagesDirectory)
        .filter((filename) => path.extname(filename).toLowerCase() === '.md')
        .filter((fileName) => !{ 'README.md': true, 'LICENSE.md': true }[fileName])
        .map(
          (fileName) => `${options.bullet} [${getTitle(fileName)}](${fileName})${options.bullet[0] === '#' ? '\n' : ''}`
        )
        .join('\n');
    },
  },
};
