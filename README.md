# Koober Coding standard

<!-- AUTO-GENERATED-CONTENT:START (INDEX:bullet=##) -->
## [1. The Zen of Koober Tech](1--The-Zen-of-Koober-Tech.md)

## [2. Git Style Guide](2--Git Style Guide.md)

## [3. Testing Standard](3--Testing Standard.md)

## [4. Typescript Standard](4--Typescript Standard.md)
<!-- AUTO-GENERATED-CONTENT:END -->
