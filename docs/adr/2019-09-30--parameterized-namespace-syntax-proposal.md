# Parameterized module syntax proposal ![Status: Draft](https://img.shields.io/badge/status-approved-green.svg)

<!---
Use shields.io generator :
![Status: Approved](https://img.shields.io/badge/status-approved-green.svg)
![Status: Draft](https://img.shields.io/badge/status-draft-blue.svg)
![Status: Rejected](https://img.shields.io/badge/status-rejected-orange.svg)
![Status: Deprecated](https://img.shields.io/badge/status-deprecated-lightgrey.svg)
-->

## Context

<!---
What is the issue that we're seeing that is motivating this decision or change.
-->
To build a functor (i.e. a parameterized module like OCaml) many syntax could be possible.
It would be good to harmonize syntax to avoid name conflicts between types, declaration and module.

Proposal Syntax #1

```ts
function Seq<T extends Seq.Type<any>>(Item: { equal(l: T, r: T): boolean }): Seq.Module<T> {
  // Private method
  function equalIterators(leftIterator, rightIterator) {
    // [...] to implement
    // Item.equal(itemLeft, itemRight)
    return false;
  }
  
  function equal(left, right) {
    return equalIterators(left.iterator(), right.iterator());
  }

  return {
    equal,
  };
}
namespace Seq {
  export type Type<T> = {
    iterator(): Iterator<T>;
  };

  export type Module<T extends Type<any>> = {
    equal(seqLeft: T, seqRight: T): void;
  };
}
```

Syntax #2

```ts

namespace Seq {
  export type Type<T> = {
    iterator(): Iterator<T>;
  };

  export type Module<T extends Type<any>> = {
    equal(seqLeft: T, seqRight: T): void;
  };

  export function Make(Item: { equal(l: T, r: T): boolean }): Module<T> {
    // Private method
    function equalIterators(leftIterator, rightIterator) {
      // [...] to implement
      // Item.equal(itemLeft, itemRight)
      return false;
    }

    function equal(left, right) {
      return equalIterators(left.iterator(), right.iterator());
    }

    return {
      equal,
    };
  }
}
```

## Decision

<!---
What is the change that we're actually proposing or doing.
-->

Syntax 1 was chosen because both are quite similar but less verbose

## Consequences

<!---
What becomes easier or more difficult to do because of this change.
-->
