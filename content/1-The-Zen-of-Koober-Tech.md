---
title: "The Zen of Koober Tech"
metaTitle: "The Zen of Koober Tech"
metaDescription: "Important values of the tech team"
---

<!-- AUTO-GENERATED-CONTENT:START (TOC:firsth1=false) -->
- [**Together** better than *Alone*](#together-better-than-alone)
- [**One obvious way to do** but *Many ways to think*](#one-obvious-way-to-do-but-many-ways-to-think)
- [**Replicating and Adapting** before *Inventing*](#replicating-and-adapting-before-inventing)
- [**Robust** is better than *Fragile*](#robust-is-better-than-fragile)
- [**Explicit** is better than *Implicit*](#explicit-is-better-than-implicit)
- [**Correct**, then **Clear**, then **Concise**, then **Fast**](#correct-then-clear-then-concise-then-fast)
- [**Simple** is better than *Complex*, better than *Complicated*](#simple-is-better-than-complex-better-than-complicated)
- [**Pure** is better than *Impure*, better than *Obscure*](#pure-is-better-than-impure-better-than-obscure)
- [**Tested** is better than *Documented*](#tested-is-better-than-documented)
- [**Fail early, Fail loudly**](#fail-early-fail-loudly)
- [**Automated** is better than *Manual*, better than *Magic*](#automated-is-better-than-manual-better-than-magic)
<!-- AUTO-GENERATED-CONTENT:END -->

[🖨 Printable version](./Zen%20of%20Koober.pdf)

## **Together** better than *Alone*

**Together as a team** : Teamwork is more valuable than individual performance. Confronting other's people advice helps solving problems quicker and better.

**Together as a safety net** : Code review is important to improve code quality and developer's skills in order to deliver a product without defects. Pair programming is even better because this review is continuous and catch defects all along the implementation process.

**Together as customer and provider** : The work of a person is often the input for another person and so on. Criticism and feedback in a respectful environment is the way to self-improvement.

## **One obvious way to do** but *Many ways to think*

We have to standardize the way to work but to keep the mind creative.

## **Replicating and Adapting** before *Inventing*

When facing a problem, there are high probabilities that somebody else faced a similar one.
This is another formulation for the [Shuhari](https://en.wikipedia.org/wiki/Shuhari) japanese concepts from martial arts.

## **Robust** is better than *Fragile*

Using the word `robust`, please include also related physical properties that could be applied to a system :

- The system behaves consistently with inconsistent state. (Robustness)

- The system behaves consistently in an inconsistent environment. (Fault Tolerance)

- The system behaves consistently even after a critical event (Resilience)

## **Explicit** is better than *Implicit*

The definition of `Implicit` is `implied or understood though not directly expressed`.
When not directly expressed, a piece of code executed can have unwanted side effects, occurring bugs with potentially large impacts and hard to debug.

## **Correct**, then **Clear**, then **Concise**, then **Fast**

Clear and concise code should not be sacrificed for the sake of efficiency. Iterating over an efficient but unreadable code is a dead end. The order of the steps of improvements is therefore important.

## **Simple** is better than *Complex*, better than *Complicated*

Simplicity is a key value for maintainability and it should be the goal to achieve.

Nevertheless, some problems are intrinsically complicated and no simple solution can be found. An acceptable solution is often to break the complicated problem into many simple problems with complex relations between them.

## **Pure** is better than *Impure*, better than *Obscure*

Purity makes the code predictable, which helps understanding, debugging, testing the code. A pure but obscure code, breaks most of these benefits.

## **Tested** is better than *Documented*

Documentation is good and should be done as much as possible. Tests could be considered as "executable documentation" : they describe *and* check programmatically how the system behaves.

## **Fail early, Fail loudly**

When a defect is found it should be reported as soon as possible and should not be ignored. This is valid for systems and human beings.

## **Automated** is better than *Manual*, better than *Magic*

Automation is the way to avoid human errors. This gives the ability to focus on the work that really needs human skills. Nevertheless, an automated process must be understandable and fixable by any qualified person and should not have "magical" behaviors.
