---
title: "Typescript Standard"
metaTitle: "Typescript Standard"
metaDescription: "Important values of the tech team"
---

## 1. Interface and Types

- [`PascalCase` name](#pascalcase)
- No prefix `I` or `T`
- No suffix `Interface` or `Type`

**Example :**

```ts
type FooBar = any;
```

## 2. Enum

- [`PascalCase` name](#pascalcase)
- [`PascalCase` values](#pascalcase)

**Example :**

```ts
enum FooBar {
  Value
}
```

## 3. Class and objects

- [`PascalCase` name](#pascalcase)
- [`camelCase` properties](#camelCase)
- No `_` for private/protected, use typescript keyword

**Example :**

```ts
class FooBar {
  myProperty: number;
  private specialValue: number;
}
```

## 4. Namespace and parameterized namespace

## 4.1 Namespace

```ts
namespace FooBar {
  export type Module = {
    myMethod(): void
  };

  function myPrivateMethod() {
  }

  export function myMethod() {
  }
}
```

## 4.2 Parameterized namespace

```ts
function Seq<T extends Seq.Type<any>>(Item: { equal(l: T, r: T): boolean }): Seq.Module<T> {
  // Private method
  function equalIterators(leftIterator, rightIterator) {
    // [...] to implement
    // Item.equal(itemLeft, itemRight)
    return false;
  }
  
  function equal(left, right) {
    return equalIterators(left.iterator(), right.iterator());
  }

  return {
    equal,
  };
}
namespace Seq {
  export type Type<T> = {
    iterator(): Iterator<T>;
  };

  export type Module<T extends Type<any>> = {
    equal(seqLeft: T, seqRight: T): void;
  };
}
```

[See proposal](docs/adr/2019-09-30--parameterized-module-syntax-proposal.md)

## 5. Constants

### 5.1 Hexadecimal notation (colors, etc.)

- `Lower case` name

**Example :**

```ts
const backgroundColor = '#fff';
```

## Addendum

### PascalCase

Words are concatenated without spaces. Each word starts with an uppercase letter.

For initials, every letters are upper cased.

**Example :**
```MyVariableName, ToHTML, HTMLText, APIQuery```

### camelCase

Words are concatenated without spaces. Each word starts with an uppercase letter except the first one.

For initials, every letters are lower cased if starting the word.

**Example :**
```myVariableName, toHTML, htmlText, apiQuery```
