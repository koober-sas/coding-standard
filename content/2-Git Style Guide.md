---
title: "Git Style Guide"
metaTitle: "Git Style Guide"
metaDescription: "Important values of the tech team"
---

## Commits

### Atomicity

Each commit should be a single *logical change*. Don't make several
*logical changes* in one commit and don't split a single *logical change* into several commits

*Explanation: In case of bug, one commit should be easily reverted. It is also easier to read in git history.*

### Frequency

Commit *early* and *often*. Small, self-contained commits are easier to understand and revert when something goes wrong.

*Explanation : frequent commits reduce the risk of difficult merges and bring the value to the team as soon as possible.*

### Reliability

Commits should never breaks the branch

*Explanation : branches are often linked to tools like CI and should always be "testable"/"runnable".*

## Commit Messages

### Summary line (first line)

- it should be *descriptive* yet *succinct*.
- it should be shorter than *50 characters*.
- it should be capitalized and written in imperative present tense.
- it should not end with a period since it is effectively the commit
  *title*:

```shell
# good - imperative present tense, capitalized, fewer than 50 characters
Mark huge records as obsolete when clearing hinting faults

# bad
fixed ActiveModel::Errors deprecation messages failing when AR was used outside of Rails.
```

### Content (optional)

- it should be separated with a blank line from the title
- it should be wrapped to *72 characters*
- it should also provide any pointers to related resources (eg. link to the corresponding issue in a bug tracker):

Example :

  ```text
  Short (50 chars or fewer) summary of changes

  More detailed explanatory text, if necessary. Wrap it to
  72 characters. In some contexts, the first
  line is treated as the subject of an email and the rest of
  the text as the body.  The blank line separating the
  summary from the body is critical (unless you omit the body
  entirely); tools like rebase can get confused if you run
  the two together.

  Further paragraphs come after blank lines.

  - Bullet points are okay, too

  - Use a hyphen or an asterisk for the bullet,
    followed by a single space, with blank lines in
    between

  The pointers to your related resources can serve as a footer
  for your commit message. Here is an example that is referencing
  issues in a bug tracker:

  Resolves: #56, #78
  See also: #12, #34

  Source http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
  ```

## Branches

### Trunk based development

For a simple / efficient continuous delivery flow we prefer the [Trunk based development](https://trunkbaseddevelopment.com/) flow

### Naming

- Choose *short* and *descriptive* names:

    ```shell
    # good
    git checkout -b oauth-migration

    # bad - too vague
    git checkout -b login-fix
    ```

- Use *-* character to separate words.

    ```shell
    # good
    git checkout -b my-feature

    # bad
    git checkout -b my_feature
    ```

- Identifiers from corresponding tickets in an external service (eg. a GitHub
  issue) are also good candidates for use in branch names. For example:

  ```shell
  # GitHub issue #15
  git checkout -b issue-15
  ```

## References

- [Agis Style Guide](https://github.com/agis/git-style-guide)
- [How to Get Your Change Into the Linux Kernel](https://kernel.org/doc/html/latest/process/submitting-patches.html)
- [git man pages](http://git-scm.com/doc)
