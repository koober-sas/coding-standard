# Given-When-Then

## Definition

Given-When-Then is a style of representing tests.

**Given** : The state of the system before the execution of the scenario

**When** : The properly executed test scenario

**Then** : The expected state of the system after the execution of the scenario

## Links

- [https://martinfowler.com/bliki/GivenWhenThen.html](https://martinfowler.com/bliki/GivenWhenThen.html)
- [http://referentiel.institut-agile.fr/gwt.html](http://referentiel.institut-agile.fr/gwt.html)
