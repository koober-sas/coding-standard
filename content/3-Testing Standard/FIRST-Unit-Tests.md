# F.I.R.S.T. Unit Tests
<!-- AUTO-GENERATED-CONTENT:START (TOC:firsth1=false) -->
- [F.I.R.S.T. Unit Tests](#first-unit-tests)
  * [Fast](#fast)
  * [Isolated](#isolated)
  * [Repeatable](#repeatable)
  * [Self-Validating](#self-validating)
  * [Thorough / Timely](#thorough--timely)
<!-- AUTO-GENERATED-CONTENT:END -->

## Fast

- Problem should be reported fast
- When tests are slow, developer tend to ignore them and then introduce bugs

## Isolated

- **1 test = 1 case**
- **Order independent** : Tests should not rely on the order of execution
- **State independent** : Tests should avoid to share state or data. Use init helpers to reduce code repetition if needed

## Repeatable

- **Deterministic** (i.e. avoid Date and random)
- **Environment independent** :  Running tests should have always same results different environments (CI, desktop, etc)

## Self-Validating

- No manual inspection to check whether the test has passed or failed.

## Thorough / Timely

- Write a **test before writing corresponding code**. It will produce code with easier testability.
- **Cover all use cases** better than **Cover all code**
- Cover boundary values and edge cases
- Cover errors and exceptions
- Cover bad inputs
