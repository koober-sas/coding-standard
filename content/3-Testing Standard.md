---
title: "Testing Standard"
metaTitle: "Testing Standard"
metaDescription: "Important values of the tech team"
---

## Purpose

In software engineering, quality is important. What is the best tool for quality ? Tests !

## Test driven development (TDD)

See [Wikipedia](https://en.wikipedia.org/wiki/Test-driven_development)

- **3 STEPS** : RED,GREEN,BLUE
  1. RED : write a test that describe the problem
  2. GREEN : write the code that solves the problem
  3. BLUE: refactor / clean
- **F.I.R.S.T. Unit Tests** : [Fast, Isolated, Repeatable, Self-Validating and Thorough](./more/FIRST-Unit-Tests.md)
- **EXPLICIT** Tests should be written in a way that any developer should understand what the target code is doing (Tests are documentation)
  * [Given, When, Then](./more/Given-When-Then.md) testing style is recommended
- **ONE BUG = ONE NEW TEST** - A bug is a use case not handled by the system. Therefore, to check that a bug was fixed, there should be a test corresponding to that new case.
- **USAGE DRIVEN** : The more a piece of code is or will be reused, the more tested the code must be.
- **IMPACT DRIVEN** : If the consequences of a bug on a piece of code could be catastrophic, then there should be tests
